package com.folcademy.bancoled;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoLedApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoLedApplication.class, args);
	}

}
